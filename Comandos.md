## Comandos

Lista de comandos utilizados durante o desenvolvimento do código

- Criar novo projeto
    
    `django-admin startproject PoliMakers .`
    
    `python manage.py createsuperuse`
    
- Criar servidor para visualizar o site
    
    `python manage.py runserver`

- Realizar migrações após criar classes em models.py
    
    `python manage.py makemigrations`
    
    `python manage.py migrate`
    
- Instalar o Bootstrap
    
    `npm install bootstrap`