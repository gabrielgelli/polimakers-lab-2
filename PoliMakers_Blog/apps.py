from django.apps import AppConfig


class PolimakersBlogConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'PoliMakers_Blog'
