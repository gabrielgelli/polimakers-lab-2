from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from PoliMakers_Blog.models import Post
from .models import *
from .forms import *
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404

# Create your views here.


class HomeView(ListView):  # Cria uma lista de views
    model = Post
    template_name = 'home.html'
    # ordering = ['-id']
    ordering = ['-post_date']


class PostPageView(DetailView):  # Detalha um view
    model = Post  # Retorna um objeto do tipo Post
    template_name = 'post_page.html'  # Indica para qual html irá olhar


class CreatePostView(CreateView):  # Cria uma nova view
    model = Post
    form_class = PostForm
    template_name = 'criar_post.html'


class DeletePostView(DeleteView):  # Deleta a view
    model = Post
    template_name = 'deletar_post.html'
    success_url = reverse_lazy('home')  # Se clicar em deletar, retorna à home


class EditPostView(UpdateView):
    model = Post
    form_class = EditForm
    template_name = 'editar_post.html'


class CreateCommentView(CreateView):
    model = Post
    form_class = CommentForm
    template_name = 'criar_comentario.html'

    def form_valid(self, form):
        form.instance.post_id = self.kwargs['pk']
        return super().form_valid(form)

    success_url = reverse_lazy('home')
