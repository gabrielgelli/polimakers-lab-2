from django.urls import path
from .views import *
from . import views

urlpatterns = [
    path("", HomeView.as_view(), name="home"),
    # name é o nome que será buscado junto com a primary key
    path("post_page/<int:pk>", PostPageView.as_view(), name="post_page"),
    path("criar_post/", CreatePostView.as_view(), name="criar_post"),
    path("post_page/<int:pk>/deletar",
         DeletePostView.as_view(), name="deletar_post"),
    path("post_page/<int:pk>/editar", EditPostView.as_view(), name="editar_post"),
    path("post/<int:pk>/comentar/",
         CreateCommentView.as_view(), name="criar_comentario"),
]
